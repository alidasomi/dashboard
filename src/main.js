import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import App from './App'
import router from './router'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import { MLInstaller, MLCreate, MLanguage } from 'vue-multilanguage'
import JQuery from 'jquery'
import './ml'
import './css/fa.css'
import './css/en.css'
import './css/style.css'
import PDatePicker from 'vue2-persian-datepicker'
import VuePlyr from 'vue-plyr'
import 'vue-plyr/dist/vue-plyr.css'

Vue.use(VuePlyr)
Vue.component('pdatepicker', PDatePicker)
Vue.config.productionTip = false
Vue.use(Vuetify)
new Vue({
  el: '#app',
  router,
  components: { App},
  template: '<App/>'
})
/////