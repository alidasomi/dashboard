import Vue from 'vue'
import { MLInstaller, MLCreate, MLanguage } from 'vue-multilanguage'

Vue.use(MLInstaller)

export default new MLCreate({
  initial: 'fa',//default language 
  save: process.env.NODE_ENV === 'production',
  languages: [
    new MLanguage('en').create({//english language
      langsel:'eng-lang-sel',
      direction:'perdirection',
      appname:'Admin panel',
      page:'pageen',
        menu: [
            { title: 'page1', icon: 'dashboard' ,url:"./page1"},
            { title: 'page2', icon: 'question_answer' ,url:"./page2"},
            { title: 'page3', icon: 'question_answer' ,url:"./page3"},
            { title: 'page4', icon: 'question_answer' ,url:"./page4"},
            { title: 'page5', icon: 'question_answer' ,url:"./page5"}
          ]
    }),

    new MLanguage('fa').create({//farsi language
      langsel:'per-lang-sel',
      direction:'engdirection',
      appname:'پنل ادمین',
      page:'pagefa',
        menu: [

            { title: 'صفحه۱', icon: 'dashboard' ,url:"./page1"},
            { title: 'صفحه۲', icon: 'question_answer' ,url:"./page2"},
            { title: 'صفخه۳', icon: 'question_answer' ,url:"./page3"},
            { title: 'صفخه۴', icon: 'question_answer' ,url:"./page4"},
            { title: 'صفخه۵', icon: 'question_answer' ,url:"./page5"}
          ]
    })
  ]
})
